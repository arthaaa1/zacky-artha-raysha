// Author : Zacky Artha Raysha
// NIM : 301230037
// Kelas : IF 1A
// UTS PRAKTIKUM ALGORITMA
// Soal No.3

#include <iostream>

using namespace std;

int main() {
    int N;

    cout << "Masukkan N: ";
    cin >> N;

    // Cetak segitiga bagian atas
    for (int i = 1; i <= N; i++) {
        // Cetak spasi di sebelah kiri
        for (int j = 1; j <= N - i; j++) {
            cout << " ";
        }

        // Cetak bintang
        for (int k = 1; k <= i; k++) {
            cout << "* ";
        }

        cout << endl;
    }

    // Cetak segitiga bagian bawah
    for (int i = N - 1; i >= 1; i--) {
        // Cetak spasi di sebelah kiri
        for (int j = 1; j <= N - i; j++) {
            cout << " ";
        }

        // Cetak bintang
        for (int k = 1; k <= i; k++) {
            cout << "* ";
        }

        cout << endl;
    }

    return 0;
}

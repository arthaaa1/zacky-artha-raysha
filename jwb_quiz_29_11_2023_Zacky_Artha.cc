/*
   Nama author  : zacky artha raysha
   Tgl buat     : 29 November 2023
   Deskripsi    : menghitung rata-rata menggunakan do-while 
*/

#include <iostream>
#include <iomanip>
#include <cstdlib>

using namespace std;

int main()
{
    system("clear");

    int i = 0, N = 0;  
    float Data = 0.0, Rata = 0.0, Total = 0.0;

    cout << "Banyaknya data : ";
    cin >> N;

    cout << "Masukan Data ke-" << i + 1 << " : ";
    cin >> Data;
    Total += Data;
    i++;

    do {
        cout << "Masukan Data ke-" << i + 1 << " : ";
        cin >> Data;
        Total += Data;
        i++;
    } while (i <= 3);

    Rata = Total / N;
    cout << "Banyaknya Data       : " << N << endl;
    cout << "Total Nilai Data     : " << fixed << setprecision(2) << Total << endl;
    cout << "Rata-rata nilai data : " << fixed << setprecision(2) << Rata << endl;

    return 0;
}
